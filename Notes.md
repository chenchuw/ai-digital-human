# Checking if a Port is in Use on Linux

In Linux, you can use the `netstat` or `ss` command to check if a port is in use.

## Using netstat

The `netstat` command can display network connections, listening ports, and the processes using them. Here's how you can use it:

```bash
netstat -tuln | grep <your-port-number>
```
<br>

# Client-Server Program

This is a simple client-server program written in Python using asyncio for asynchronous networking.

## Files

- `server.py`: This is the server script. It listens for incoming connections and handles messages from clients.
- `client.py`: This is the client script. It connects to the server and sends messages.

## How to Use

1. Start the server: Run `python server.py` in your terminal. The server will start and listen for incoming connections.

2. Start the client: In a new terminal window, run `python client.py`. The client will connect to the server.

3. Send messages: In the client terminal, you can type messages and press enter to send them to the server. The server will echo back the messages.

## Commands

- To exit the client, type 'exit' and press enter. This will close the client connection.
- To shut down the server, send the message 'shutdown' from the client. This will stop the server from accepting new connections and shut it down.

## Error Handling

The client script has built-in error handling for connection errors. If the server is not running when you start the client, it will print an error message: "Could not connect to the server. Is it running?"

## Note

The client script will not send empty messages. If you try to send an empty message, it will print a warning: "Cannot send an empty message", and prompt you to enter a new message.
<br>