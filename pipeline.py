import torch
from torch.autograd import Variable
from model1 import Model1  # Import the classes for your models
# from model2 import Model2
# from model3 import Model3

"""This is a template for the Pipeline implementation."""

class ADH_Pipeline:
    def __init__(self, model_paths="./model.ts"):
        # self.models = [Model1(model_paths['model1']), Model2(model_paths['model2']), Model3(model_paths['model3'])]
        self.models = []
        self.module_names = ["KWS", "ASR", "chatGLM2", "TTS", "MGM", "Wav2Lip"]
        self.user_audio_input = None
        self.user_avatar_input = None
        self.video_frames_output = None

        for model in self.models:
            model.eval()  # Set the model to evaluation mode

    def process_input(self, input_data):
        # Convert the input data to a PyTorch Variable
        processed_data = Variable(torch.FloatTensor(input_data))
        return processed_data

    def run(self, input_data):
        processed_data = self.process_input(input_data) # Process the audio received from client end
        
        '''Implementation of model assembly...'''

        pass

    def process_output(self, output_data):
        # Convert the output to a format that can be feed to the web app
        
        '''Implementation of data format transfer...'''

        pass